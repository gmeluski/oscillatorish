import { AudioContext } from 'node-web-audio-api'

const FREQUENCY = 440;
const AUDIO_CHANNELS = 1;
const SAMPLE_RATE = 44100;
const SECONDS = 2;

const getAngularFrequency = (timeFrequency) => {
  return timeFrequency * 2 * Math.PI;
}

const generateSample = (sampleNumber) => {
  const sampleTime = sampleNumber / SAMPLE_RATE;
  const sampleAngle = sampleTime * getAngularFrequency(FREQUENCY);
  return Math.sin(sampleAngle);
}

const sineContext = new AudioContext();
const sineBuffer = sineContext.createBuffer(AUDIO_CHANNELS, SAMPLE_RATE * SECONDS, SAMPLE_RATE);
const bufferArray = sineBuffer.getChannelData(0);
for (let i = 0; i < SAMPLE_RATE * SECONDS; i++) {
   bufferArray[i] = generateSample(i);
}

let source = sineContext.createBufferSource();
source.buffer = sineBuffer;
source.connect(sineContext.destination);
source.start(sineContext.currentTime);
