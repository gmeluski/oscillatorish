use std::fs::File;
use std::f32::consts::PI;
use web_audio_api::context::{BaseAudioContext, AudioContext, AudioContextOptions};
use web_audio_api::node::{AudioNode, AudioScheduledSourceNode};

const AUDIO_CHANNELS: usize = 1;
const SAMPLE_RATE_F32: f32 = 44100.;
const FREQUENCY_F32: f32 = 440.;

fn get_sample_time(sample_position: usize, sample_rate: usize) -> f32 {
    return sample_position as f32 / sample_rate as f32
}

fn get_angular_frequency(time_frequency: f32) -> f32 {
    return time_frequency as f32 * PI * 2.;
}

fn main() {
    let opts = AudioContextOptions {
        sample_rate: Some(SAMPLE_RATE_F32),
        ..AudioContextOptions::default()
    };
    let context = AudioContext::new(opts);

    let length = context.sample_rate() as usize;
    let sample_rate = context.sample_rate();
    let mut buffer = context.create_buffer(AUDIO_CHANNELS, length, sample_rate);

    // fill buffer with a sine wave
    let mut sine = vec![];

    for i in 0..length {
        let phase = get_sample_time(i, length) * get_angular_frequency(FREQUENCY_F32);
        sine.push(phase.sin());
    }

    buffer.copy_to_channel(&sine, 0);

    // play the buffer in a loop
    let src = context.create_buffer_source();
    src.set_buffer(buffer.clone());
    src.set_loop(true);
    src.connect(&context.destination());
    src.start();
    loop {}
}
